package day1;

public class Main {
	
	public static void main(String[] args) {
		
		int jefSarwoSetyo = 12;
		char i = 'i';
		float phi = 3.14f;
		double ipk = 3.7;
		String ganteng = "Achenk";
		boolean benar = true;
			
		System.out.println(jefSarwoSetyo);
		System.out.println(phi);
	
		String nama, alamat;
		int usia;
		double tinggi;
		
		nama = "Arya";
		alamat = "Ciledug";
		usia = 17;
		tinggi = 169;
		
		System.out.println("Nama Kita : " + nama);
		System.out.println("Alamat : " + alamat);
		System.out.println("Usia : " + usia);
		System.out.println("Tinggi Badan : " + tinggi);
		
		int nilaiSatu = 10, nilaiDua = 5;
		System.out.println("Jumlah nilaiSatu dan nilaiDua : " + (nilaiSatu + nilaiDua));
		
		//Operasi Matematika: +,-,/,*,%,^
		//Operasi Logika: ==, !=, <, <=, >, >=, &&, ||
		//Operasi Penugasan: =, +=, ++, etc..
		
		//logic disini
		
		nilaiSatu = nilaiSatu + nilaiDua;
		nilaiDua = nilaiSatu - nilaiDua;
		nilaiSatu = nilaiSatu - nilaiDua;
		
		
		System.out.println("Nilai Satu: "+ nilaiSatu);
		System.out.println("Nilai Dua: "+ nilaiDua);
	}
}
