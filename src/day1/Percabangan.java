package day1;

import java.util.Scanner;

public class Percabangan {

	// Operasi Logika: ==, !=, <, <=, >, >=, &&, ||

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// Batch 289 lulus jika memiliki nilai lebih besar
		// atau sama dengan 75
		System.out.println("Masukkan Nomer Ruangan");
		int ruangan = input.nextInt();
		
		input.nextLine();
		System.out.println("Masukkan Nama Siswa");
		String nama = input.nextLine();
		System.out.println("Masukkan Nilai Siswa: ");
		int nilai = input.nextInt();
		System.out.println("Masukkan Nilai Softskill Siswa: ");
		int nilaiSoftSkill = input.nextInt();
		
		input.nextLine();
		System.out.println("Masukkan Batch");
		String batch = input.nextLine(); //untuk menginput string yang bisa menggunakan spasi
		
		if (nilai >= 75 && nilaiSoftSkill >= 3) { // true
			//statement
			System.out.println(nama + " " + batch + " " + " Lulus Bootcamp!! " + ruangan);
		} else {
			System.out.println(nama + " " + batch + " " + " Tidak Lulus Botcamp " + ruangan);
		}
	}
}
